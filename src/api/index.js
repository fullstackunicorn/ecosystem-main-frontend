import account from './account/index.js';
import main from './main/index.js';

const api = {
    
    account,
    main,

    url: {
        route:'',
    },

    user: {
        id:'',
    },
};
export default api;