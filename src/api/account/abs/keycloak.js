import Keycloak from 'keycloak-js';

console.log(
  '\n keycloak variables',
  '\n base url:  ', process.env.REACT_APP_KEYCLOAK_BASE_URL, 
  '\n realm:     ', process.env.REACT_APP_KEYCLOAK_REALM,
  '\n client id: ', process.env.REACT_APP_KEYCLOAK_CLIENT_ID, 
)

const keycloak = new Keycloak({

  'url': `${process.env.REACT_APP_KEYCLOAK_BASE_URL}auth`, 
  'realm': process.env.REACT_APP_KEYCLOAK_REALM, 
  'clientId': process.env.REACT_APP_KEYCLOAK_CLIENT_ID,
    // ?? 
  'ssl-required': 'none', //'ssl-required': 'external',
  'resource': 'Develop',
  'public-client': true,
  'confidential-port': 0,
  'checkLoginIframe':false,
});

export default keycloak;
