import Fetch from './Fetch/index.js';
import Create from './Create/index.js';
import NotFound from './NotFound/index.js';
import Token from './Token/index.js';
import { Suspense } from 'react';

const Page = (props) => {
    const page = {
        fetch: <Fetch/>,
        create: <Create/>,
        notFound: <NotFound/>,
        token: <Token/>,
    };
    return (
        <Suspense fallback={
            <>
                <br/>
                <div className='block'>
                    <div className='line'>
                        <div className='value center'>Loading...</div>
                    </div>
                </div>
            </>
        }>
            {page[props.render]}
        </Suspense>
    
    );
}; 
export default Page;