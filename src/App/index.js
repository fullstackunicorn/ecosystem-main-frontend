import {Routes, Route, useLocation} from 'react-router-dom';
import api from '../api/index.js';
import Layout from './Layout/index.js';
import Page from './Page/index.js';

const App = () => { 

    api.url.route = useLocation().pathname;

    return (
        <div className='app'>
            <Routes>
                <Route path='' element={<Layout/>}>
                    <Route path='/' element={<Page render='create'/>}/>
                    <Route path='/fetch' element={<Page render='fetch'/>}/>
                    <Route path='/token' element={<Page render='token'/>}/>
                    <Route path='/*' element={<Page render='notFound'/>}/>
                </Route>
            </Routes>
        </div>
    );
};
export default App;